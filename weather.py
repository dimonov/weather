import sys, json, requests

#main
if (len(sys.argv) < 2):
    print("Usage: python3 weather.py YOUR-LOCATION")
    quit()

#define
key = ""
loc = " ".join(str(c) for c in sys.argv[1:])

wu = "http://api.wunderground.com/api/{}/alerts/conditions/q/{}.json".format(key, loc)

rqe = requests.get(wu)
w = json.loads(rqe.text)

full = w["current_observation"]["display_location"]["full"]
cc = w["current_observation"]["display_location"]["country_iso3166"]
temp_f = w["current_observation"]["temp_f"]
temp_c = w["current_observation"]["temp_c"]
feelslike_f = w["current_observation"]["feelslike_f"]
feelslike_c = w["current_observation"]["feelslike_c"]
humidity = w["current_observation"]["relative_humidity"]
pressure = w["current_observation"]["pressure_mb"]
wind_string = w["current_observation"]["wind_string"]
wind_dir = w["current_observation"]["wind_dir"]
wind_kph = w["current_observation"]["wind_kph"]
wind_gust_kph = w["current_observation"]["wind_gust_kph"]
local_time = w["current_observation"]["local_time_rfc822"]
local_tz_short = w["current_observation"]["local_tz_short"]

weather = "{}, {}. {}*C ({}*F). Feels like {}*C ({}*F). Humidity: {}. Pressure: \
{} mb. Wind: {} from the {} at {} KM/H gusting to {} KM/H. \
Local time: {} {}".format(full, cc, temp_c, temp_f, feelslike_c, \
feelslike_f, humidity, pressure, wind_string, wind_dir, wind_kph, \
wind_gust_kph, local_time, local_tz_short)

print(weather)
